package calc

import (
	"testing"
)

func TestNew(t *testing.T) {
	checks := [...]struct {
		name      string
		strategy  string
		expectErr bool
	}{
		{"empty", "", true},
		{StrategyLines, StrategyLines, false},
		{StrategyStatements, StrategyStatements, false},
		{VHead, VHead, false},
	}

	for _, check := range checks {
		t.Run(check.name, func(t *testing.T) {
			_, actualErr := New(check.strategy)
			if (actualErr != nil) != check.expectErr {
				t.Fatalf("got error %v, but expected %v", actualErr, check.expectErr)
			}
		})
	}
}
