// Package calc contains the various coverage calculator implementations.
package calc

import (
	_ "embed"
	"fmt"
	"math"

	"golang.org/x/tools/cover"
)

const VHead = "latest"

//go:embed testdata/issue5-failing.txt
var Issue5Failing []byte

type Calculator interface {
	CoverageByPackage([]*cover.Profile) CoverageSummary
}

type CoverageCounts struct{ Covered, Uncovered int }

func (cc CoverageCounts) Decimal() int {
	return int(math.Round(1000 * cc.Float()))
}

func (cc CoverageCounts) Float() float64 {
	cov := float64(cc.Covered)
	unc := float64(cc.Uncovered)
	ratio := cov / (cov + unc)
	return ratio
}

// CoverageByFile represents the statement coverage after parsing as a map[package]map[file]coverage.
type CoverageByFile map[string]map[string]CoverageCounts

type CoverageSummaryElement struct {
	Package  string
	Coverage int // Rounded integer ratio to 1000, e.g. 0.9016 -> 902.
	CoverageCounts
}

// CoverageSummary is a per-package coverage summary
type CoverageSummary []CoverageSummaryElement

func New(version string) (Calculator, error) {
	var (
		c   Calculator
		err error
	)
	switch version {
	case StrategyStatements, VHead:
		c = new(calculatorPerStatement)
	case StrategyLines:
		c = new(calculatorPerLine)
	default:
		err = fmt.Errorf("unknown calculator strategy %s", version)
	}
	return c, err
}
