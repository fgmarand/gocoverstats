package calc

import (
	"math"
	"path/filepath"
	"sort"
	"strings"

	"golang.org/x/tools/cover"
)

const StrategyLines = "lines"

// CoverageByFileLines represents the line coverage after parsing as a map[package]map[file]map[line]covered.
type CoverageByFileLines map[string]map[string]map[int]bool

type calculatorPerLine struct{}

// calcCoverageByFile synthesizes the passed per-line coverage to a per-file summary.
func (calculatorPerLine) calcCoverageByFile(profs []*cover.Profile) CoverageByFileLines {
	work := CoverageByFileLines{}
	for _, prof := range profs {
		dir := filepath.Dir(prof.FileName)
		pkg := dir // TODO add naming protection logic for package name
		if work[pkg] == nil {
			work[pkg] = map[string]map[int]bool{}
		}
		if work[pkg][prof.FileName] == nil {
			work[pkg][prof.FileName] = map[int]bool{}
		}

		for _, block := range prof.Blocks {
			// Coverage merges.
			if block.Count > 0 {
				for line := block.StartLine; line <= block.EndLine; line++ {
					work[pkg][prof.FileName][line] = true
				}
				continue
			}
			// Non-coverage does not merge: only add not-yet counted lines.
			for line := block.StartLine; line <= block.EndLine; line++ {
				if _, ok := work[pkg][prof.FileName][line]; ok {
					continue
				}
				work[pkg][prof.FileName][line] = false
			}
		}
	}
	return work
}

// CoverageByPackage synthetises per-file coverage to a per-package summary.
func (c calculatorPerLine) CoverageByPackage(profs []*cover.Profile) CoverageSummary {
	bf := c.calcCoverageByFile(profs)
	res := make(CoverageSummary, 0, len(bf))
	for pkg, files := range bf {
		var y, n float64
		for _, file := range files {
			for _, set := range file {
				if set {
					y++
				} else {
					n++
				}
			}
		}
		coverage := int(math.Round(1000 * y / (y + n)))
		res = append(res, CoverageSummary{{pkg, coverage, CoverageCounts{
			Covered:   int(y),
			Uncovered: int(n),
		}}}...)
	}
	sort.Slice(res, func(i, j int) bool {
		return strings.Compare(res[i].Package, res[j].Package) < 0
	})
	return res
}
