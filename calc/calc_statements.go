package calc

import (
	_ "embed"
	"path/filepath"
	"sort"
	"strings"

	"golang.org/x/tools/cover"
)

const StrategyStatements = "statements"

type calculatorPerStatement struct{}

// calcCoverageByFile synthesizes the passed per-line coverage to a per-file summary.
func (calculatorPerStatement) calcCoverageByFile(profs []*cover.Profile) CoverageByFile {
	work := CoverageByFile{}
	for _, prof := range profs {
		dir := filepath.Dir(prof.FileName)
		pkg := dir // TODO add naming protection logic for package name
		if work[pkg] == nil {
			work[pkg] = map[string]CoverageCounts{}
		}

		for _, block := range prof.Blocks {
			cs := work[pkg][prof.FileName]
			if block.Count > 0 {
				cs.Covered += block.NumStmt
			} else {
				cs.Uncovered += block.NumStmt
			}
			work[pkg][prof.FileName] = cs
		}
	}
	return work
}

// CoverageByPackage synthetises per-file coverage to a per-package summary.
func (c calculatorPerStatement) CoverageByPackage(profs []*cover.Profile) CoverageSummary {
	bf := c.calcCoverageByFile(profs)
	res := make(CoverageSummary, 0, len(bf))
	for pkg, files := range bf {
		var count CoverageCounts
		for _, file := range files {
			count.Covered += file.Covered
			count.Uncovered += file.Uncovered
		}
		res = append(res, CoverageSummary{{Package: pkg, Coverage: count.Decimal(), CoverageCounts: count}}...)
	}
	sort.Slice(res, func(i, j int) bool {
		return strings.Compare(res[i].Package, res[j].Package) < 0
	})
	return res
}
