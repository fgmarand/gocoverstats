package calc

import (
	"bytes"
	"strconv"
	"testing"

	"golang.org/x/tools/cover"
)

// PerLineIterations exists because in early versions the code once produced
// non-reproducible results across repeated runs.
//
// Deprecated: this covered an old bug and will not be relevant in future versions.
const PerLineIterations = 100

func loadIssue5(t *testing.T) []*cover.Profile {
	profs, err := cover.ParseProfilesFromReader(bytes.NewBuffer(Issue5Failing))
	if err != nil {
		t.Fatalf("unexpected embed parse failure: %v", err)
	}
	return profs
}

func Test_calcPerLineCalcCoverageByFile(t *testing.T) {
	t.Parallel()
	for i := 0; i < PerLineIterations; i++ {
		i := i
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Parallel()
			calc := &calculatorPerLine{}
			cbf := calc.calcCoverageByFile(loadIssue5(t))
			if len(cbf) != 2 {
				t.Fatalf("expected 2 files, got %d", len(cbf))
			}
			for name, pkg := range cbf {
				if len(pkg) != 1 {
					t.Fatalf("expected pkg %s to contain 1 file, found %d", name, len(pkg))
				}

				for path, file := range pkg {
					if len(file) != 3 {
						t.Fatalf("expected file %s to contain 3 lines, found %d", path, len(file))
					}
					switch name {
					case ".":
						for line, covered := range file {
							if covered {
								t.Errorf("expected %s#%d to be uncovered", path, line)
							}
						}
					case "a":
						for line, covered := range file {
							if !covered {
								t.Errorf("expected %s#%d to be uncovered", path, line)
							}
						}
					}
				}
			}
		})
	}
}

func Test_calcPerLineCoverageByPackage(t *testing.T) {
	t.Parallel()
	for i := 0; i < PerLineIterations; i++ {
		i := i
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			t.Parallel()
			calc := calculatorPerLine{}
			cbp := calc.CoverageByPackage(loadIssue5(t))
			a, c := cbp[0], cbp[1]
			if a.Package != "a" {
				t.Fatalf("expected package a, got %s", a.Package)
			}
			if c.Package != "c" {
				t.Fatalf("expected package c, got %s", c.Package)
			}
			if a.Coverage != 1000 || c.Coverage != 0 {
				t.Fatalf("unexpected coverage: a: want 1000 got %d t: want 0 got %d", a.Coverage, c.Coverage)
			}
		})
	}
}
