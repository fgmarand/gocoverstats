/*
Package main contains the entry point for the tool and the display mechanism.

The command logic is in cmd/ and the coverage calculation in the calc/ directory.
*/
package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/fgmarand/gocoverstats/cmd"
)

func main() {
	var c = cmd.NewConfig()
	byPackage, err := cmd.Run(c, flag.CommandLine, os.Args[1:])
	if err != nil {
		log.Fatalln(err)
	}
	Output(os.Stdout, byPackage, c.Strategy, c.Percent, c.IsVerbose)
}
