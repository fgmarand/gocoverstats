package main

import (
	"fmt"
	"io"
	"text/tabwriter"

	"gitlab.com/fgmarand/gocoverstats/calc"
)

func formatOutputValue(value float64, inPercent bool, isVerbose bool) string {
	decimalPlaces := 3
	decimalWidth := 6
	suffix := ""

	if inPercent {
		// To keep the same alignment position for % as it does without the
		// decimal places must be reduced and decimal width increased to
		// handle 100.00%.
		decimalPlaces = 2
		decimalWidth = 7
		suffix = "%"
		value *= 100
	}

	if isVerbose {
		return fmt.Sprintf("%*.*f%s", decimalWidth, decimalPlaces, value, suffix)
	}

	return fmt.Sprintf("%.*f%s", decimalPlaces, value, suffix)
}

// Output emits the results contained in the received CoverageSummary to the passed io.Writer.
//
// If verbose is true, it will display a per-package coverage table,
// otherwise it will just emit the coverage in decimal format.
func Output(w io.Writer, byPackage calc.CoverageSummary, strategy string, inPercent bool, isVerbose bool) {
	tw := tabwriter.NewWriter(w, 0, 0, 1, ' ', 0)
	counts := calc.CoverageCounts{}
	for _, coverage := range byPackage {
		if isVerbose {
			_, _ = fmt.Fprintf(tw, "%s\t%s\n", coverage.Package,
				formatOutputValue(coverage.Float(), inPercent, isVerbose))
		}

		counts.Covered += coverage.Covered
		counts.Uncovered += coverage.Uncovered
	}

	if isVerbose {
		_, _ = fmt.Fprintf(tw, "%s\t%s of %s\n", "Global weighted coverage:",
			formatOutputValue(counts.Float(), inPercent, isVerbose), strategy)
	} else {
		_, _ = fmt.Fprintf(tw, "%s\n", formatOutputValue(counts.Float(), inPercent, isVerbose))
	}

	_ = tw.Flush()
}
