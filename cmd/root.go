// Package cmd contains the command implementations.
package cmd

import (
	"flag"
	"fmt"
	"log"

	"golang.org/x/tools/cover"

	"gitlab.com/fgmarand/gocoverstats/calc"
)

type Config struct {
	Path      string
	IsVerbose bool
	Strategy  string
	Percent   bool
}

func NewConfig() *Config {
	return &Config{Strategy: calc.StrategyStatements}
}

// Run performs the actual work of the command: loading the coverage file
// and analysing it.
func Run(pc *Config, fs *flag.FlagSet, args []string) (calc.CoverageSummary, error) {
	fs.StringVar(&pc.Path, "f", "coverage.txt", "The name of the coverage file to parse")
	fs.BoolVar(&pc.IsVerbose, "v", false, "Be verbose")
	fs.BoolVar(&pc.Percent, "percent", false, "report coverage in a percentage format")
	strategy := fs.Bool("lines", false, "report line-based coverage instead of statement-based coverage")
	if err := fs.Parse(args); err != nil {
		return nil, fmt.Errorf("parsing command line: %w", err)
	}
	if *strategy {
		pc.Strategy = "lines"
	}
	profs, err := cover.ParseProfiles(pc.Path)
	if err != nil {
		return nil, fmt.Errorf("loading coverage file %s: %w", pc.Path, err)
	}
	if pc.IsVerbose {
		log.Printf("loaded %d profiles, using %s strategy", len(profs), pc.Strategy)
	}

	c, err := calc.New(pc.Strategy)
	if err != nil {
		return nil, err
	}
	byPackage := c.CoverageByPackage(profs)
	return byPackage, nil
}
