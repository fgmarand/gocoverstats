package cmd

import (
	"flag"
	"log"
	"strings"
	"testing"
)

func TestWork(t *testing.T) {
	pc := NewConfig()
	checks := [...]struct {
		name              string
		args              []string
		expectError       bool
		expectDebugOutput bool
		expectErrorOutput bool
	}{
		{"happy", []string{"-f", "../calc/testdata/issue5-failing.txt"}, false, false, false},
		{"happy chatty", []string{"-v", "-f", "../calc/testdata/issue5-failing.txt"}, false, true, false},
		{"sad bad flag", []string{"-g"}, true, false, true},
		{"sad bad file", []string{"-f", "../calc/testdata/issue5-success.txt"}, true, false, false},
	}
	for _, check := range checks {
		check := check
		t.Run(check.name, func(t *testing.T) {
			dW := strings.Builder{}
			eW := strings.Builder{}
			fs := flag.NewFlagSet("test flags", flag.ContinueOnError)
			log.SetOutput(&dW) // Capture normal stderr logging outside test and flag parsing
			fs.SetOutput(&eW)  // Capture flag parsing messages
			_, err := Run(pc, fs, check.args)
			if err != nil && !check.expectError {
				t.Fatalf("Unexpected error: %v", err)
			}
			if err == nil && check.expectError {
				t.Fatalf("Unexpected success")
			}
			dO, eO := dW.String(), eW.String()
			if check.expectDebugOutput != (dO != "") {
				t.Fatalf("Debug output: got %q, but expected %t", dO, check.expectDebugOutput)
			}
			if check.expectErrorOutput != (eO != "") {
				t.Fatalf("Error output: got %q but expected %t", dO, check.expectErrorOutput)
			}
		})
	}
}
