# Change log
## v0

- v0.0.4: 
  - Made global coverage weighted for both strategies
  - Calculations can now be written in percentage format with `-percent`
  - Verbose final line output is now formatted as `Global weighted coverage: x.xxx of [strategy]` based on selected strategy
  - Gitlab coverage badge now correctly reflects repository test coverage percentage
- v0.0.3:
  - New statement-based calculation giving the same results as `go test -cover` 
  - Per-line calculation now behind the `-lines` CLI flag
  - Go 1.18, golang.org/x/tools/cover 0.1.10
- v0.0.2:
  - invalid version, retracted
- v0.0.1: 
  - First version.
  - Line-based calculation
