# gocoverstats

Produce aggregate coverage statistics for CI integration of Go projects.

[![pipeline status](https://gitlab.com/fgmarand/gocoverstats/badges/main/pipeline.svg)](https://gitlab.com/fgmarand/gocoverstats/-/commits/main)
[![coverage report](https://gitlab.com/fgmarand/gocoverstats/badges/main/coverage.svg)](https://gitlab.com/fgmarand/gocoverstats/-/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/fgmarand/gocoverstats)](https://goreportcard.com/report/gitlab.com/fgmarand/gocoverstats)

## What does it do ?

[gocoverstats](https://gitlab.com/fgmarand/gocoverstats) 
produces aggregated coverage information by parsing standard `go test` coverage files
to produce a global coverage ratio like the one emitted by `go test` itself, 
and optionally a per-package coverage report.

## Why is it needed ?

It answers the needs of teams maintaining projects with an existing metrics toolchain, 
and who want to track their coverage over time, globally or per-package, 
while not wishing to use complete/complex coverage analysis solutions, 
and more specifically SaaS solutions like CodeClimate, CodeCov, or Coveralls.

This is likely to happen most often for proprietary code to avoid the cost and
safety issues of providing access to such third parties, and without the need
for an on-premises complete code coverage solution.

## What does the aggregate ratio represent ?

The single ratio produced without `-v` or at the last line of the ratio table
with `-v`, called `Global weighted coverage` is a weighted average of package ratios,
accounting for the size of each package.

## How do I use gocoverstats ?

➠ Want a long-form article ? Read it in [english](https://osinet.fr/go/en/articles/plotting-go-test-coverage/) or [french](https://osinet.fr/go/articles/tracer-couverture-tests/).

### Simple use

Just 3 steps:

```bash
$ go install gitlab.com/fgmarand/gocoverstats@latest         # Download the tool
$ go test -coverprofile=coverage.txt -covermode=atomic ./... # Build a coverage file
$ gocoverstats -f coverage.txt                               # Obtain a simple coverage ratio by statements
0.938
$ gocoverstats -f coverage.txt -percent                      # Obtain a simple coverage ratio by statements with percentage 
93.81%
$ gocoverstats -f coverage.txt -lines                        # Or obtain a simple coverage ratio by lines
0.934
$ gocoverstats -f coverage.txt -lines -percent               # Obtain a simple coverage ratio by lines with percentage 
93.37%
$ gocoverstats -v -f coverage.txt                            # Or a per-package table of ratios
2022/05/01 09:02:07 loaded 2 profiles, using statements strategy  # That line is on stderr
github.com/owner/repo/package1 0.950                         # The other ones are on stdout
github.com/owner/repo/package2 0.981
Global weighted coverage:      0.953 of statements
$
```

### Integration with DataDog or Graphite metrics

- Install the Datadog Python library, following 
  [their instructions](https://github.com/DataDog/datadogpy#installation)
- Configure your `~/.dogrc`, or the `DATADOG_API_KEY` environment variable.
- To post the single aggregate as a simple metric:

```
dog metric post foo_project.coverage $(gocoverstats -f coverage.txt)
```
- To post the per-package metrics as tags on a given metric, without the
  aggregate, you can use something like this:

```bash
# This example uses grep to support both Linux and macOS.
coverage=$(gocoverstats -v -f coverage.txt | grep -v "Global weighted coverage" | tr -s '[:blank:]' ',')
for package in $coverage
do 
    IFS=, 
    read TAG RATIO <<< "${package}"
    dog metrics post foo_project.coverage "${RATIO}" --tags package:"${TAG}"
done
```

You can than graph all of these metrics in a dashboard by generating curves on 
the `package` tag of the `foo_project.coverage` metric.

For more details and for the Graphite integration, follow the steps outlined
in [this article](https://osinet.fr/go/en/articles/plotting-go-test-coverage/).


## What commands are provided by the tool ?

When installing with `go install`, nothing but the `gocoverstats` command itself.

When working on a clone of the repo, you get a [Makefile](https://gitlab.com/fgmarand/gocoverstats/-/blob/main/Makefile)
with the following tasks:

- `clean`: remove generated files like coverage and local builds
- `install`: build and install the command to `$GOBIN`
- `lint`: perform static analysis tasks on the project
- `test-native`: updates the `calc/testdata` fixtures with the coverage for the
  current version of GoCoverStats. 
- `cover`: run the test suite, generating coverage, and cleaning after itself
- `run`: build and run the project itsef `testdata`, to demonstrate usage and
  the difference between per-statement and per-line coverage.

## Support

For any question, use the Gitlab [issue tracker for the project](https://gitlab.com/fgmarand/gocoverstats/-/issues).

If you want to warn about a security issue, or anything confidential, 
be sure to check the checkbox marking the issue as confidential on the issue form.

## Roadmap

Future features being imagined, with no landing date defined:

- output formats
- badge generation

## Contributing

This project is open to contributions. Be aware that the project is Open Source,
so any code you contribute must be under the project license.

## License

This project is published under the MIT License, as 
[provided in the repository](https://gitlab.com/fgmarand/gocoverstats/-/blob/main/LICENSE).

## Credits

Gopher with a medium head coverage ratio by Renee French, under 
[Creative Commons Attribution 3.0 license](https://blog.golang.org/gopher).
