package main

import (
	"bytes"
	_ "embed"
	"fmt"
	"log"
	"os"
	"testing"

	"golang.org/x/tools/cover"

	"gitlab.com/fgmarand/gocoverstats/calc"
)

var profs []*cover.Profile

func ExampleOutput_verbose() {
	c, _ := calc.New(calc.VHead)
	cbp := c.CoverageByPackage(profs) // Assume correct: test-covered.
	Output(os.Stdout, cbp, calc.StrategyStatements, false, true)
	// Output:
	// a                          1.000
	// c                          0.000
	// Global weighted coverage:  0.500 of statements
}

func ExampleOutput_verbose_percent() {
	c, _ := calc.New(calc.VHead)
	cbp := c.CoverageByPackage(profs) // Assume correct: test-covered.
	Output(os.Stdout, cbp, calc.StrategyStatements, true, true)
	// Output:
	// a                          100.00%
	// c                            0.00%
	// Global weighted coverage:   50.00% of statements
}

func ExampleOutput_verbose_lines() {
	c, _ := calc.New(calc.StrategyLines)
	cbp := c.CoverageByPackage(profs) // Assume correct: test-covered.
	Output(os.Stdout, cbp, calc.StrategyLines, false, true)
	// Output:
	// a                          1.000
	// c                          0.000
	// Global weighted coverage:  0.500 of lines
}

func ExampleOutput_verbose_lines_percent() {
	c, _ := calc.New(calc.StrategyLines)
	cbp := c.CoverageByPackage(profs) // Assume correct: test-covered.
	Output(os.Stdout, cbp, calc.StrategyLines, true, true)
	// got:
	// a                          100.00%
	// c                            0.00%
	// Global weighted coverage:   50.00% of lines
}

func ExampleOutput_quiet() {
	c, _ := calc.New(calc.VHead)
	cbp := c.CoverageByPackage(profs) // Assume correct: test-covered.
	Output(os.Stdout, cbp, calc.StrategyStatements, false, false)
	// Output:
	// 0.500
}

func ExampleOutput_quiet_percent() {
	c, _ := calc.New(calc.VHead)
	cbp := c.CoverageByPackage(profs) // Assume correct: test-covered.
	Output(os.Stdout, cbp, calc.StrategyStatements, true, false)
	// Output:
	// 50.00%
}

func ExampleOutput_quiet_lines() {
	c, _ := calc.New(calc.StrategyLines)
	cbp := c.CoverageByPackage(profs) // Assume correct: test-covered.
	Output(os.Stdout, cbp, calc.StrategyLines, false, false)
	// Output:
	// 0.500
}

func ExampleOutput_quiet_lines_percent() {
	c, _ := calc.New(calc.StrategyLines)
	cbp := c.CoverageByPackage(profs) // Assume correct: test-covered.
	Output(os.Stdout, cbp, calc.StrategyLines, true, false)
	// Output:
	// 50.00%
}

func init() {
	var err error
	profs, err = cover.ParseProfilesFromReader(bytes.NewBuffer(calc.Issue5Failing))
	if err != nil {
		log.Fatalf("unexpected embed parse failure: %v", err)
	}
}

func Test_formatOutputValue(t *testing.T) {
	type args struct {
		value     float64
		inPercent bool
		isVerbose bool
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "should return formatted float with three decimal places",
			args: args{
				value:     0.1236,
				inPercent: false,
				isVerbose: false,
			},
			want: "0.124",
		},
		{
			name: "should return formatted float with three decimal places and 6 width when isVerbose is true",
			args: args{
				value:     0.1236,
				inPercent: false,
				isVerbose: true,
			},
			want: fmt.Sprintf("%6.3f", 0.124),
		},
		{
			name: "should return formatted float with % and two decimal places if inPercent is true",
			args: args{
				value:     0.1235,
				inPercent: true,
				isVerbose: false,
			},
			want: "12.35%",
		},
		{
			name: "should return formatted float with % and two decimal places and 7 width when isVerbose is true and isPercent is true",
			args: args{
				value:     0.1236,
				inPercent: true,
				isVerbose: true,
			},
			want: fmt.Sprintf("%7.2f%%", 12.36),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := formatOutputValue(tt.args.value, tt.args.inPercent, tt.args.isVerbose); got != tt.want {
				t.Errorf("formatOutputValue() = %v, want %v", got, tt.want)
			}
		})
	}
}
