COVERAGE_CI = coverage.txt
COVERAGE_REF = calc/testdata/coverage.txt
NATIVE = calc/testdata/native.txt
PACKAGES = $(shell go list ./... | grep -v /vendor/)

.PHONY: clean
clean:
	rm -fr gocoverstats

.PHONY: install
install:
	CGO_ENABLED=0 go install -trimpath -ldflags="-s -w" .

.PHONY: lint
lint:
	go fmt $(PACKAGES)
	go vet $(PACKAGES)
	staticcheck -checks all $(PACKAGES)

.PHONY: test-native
test-native:
	GOCOVERSTATS_UPDATE=1 go test -race -covermode=atomic -coverprofile=$(COVERAGE_REF) ./... > $(NATIVE)

cover:
	go test -race -v -coverprofile=$(COVERAGE_CI) -covermode=atomic ./... 2>&1 | grep -vE '(RUN|PAUSE|CONT|    ---)'
	go run . -v -f $(COVERAGE_CI)
	make clean

run:
	go run . -v -f calc/testdata/coverage.txt
	go run . -v -f calc/testdata/coverage.txt -lines
